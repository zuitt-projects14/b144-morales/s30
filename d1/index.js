const express = require("express");
//Mongoose is a package that allows creation of schemas to model our data structures and to have an access to a number of methods for manipulating database.
const mongoose = require("mongoose");


const app = express();
const port = 3001;

//MongoDB connection
//Connecting to mongoDB atlas

mongoose.connect("mongodb+srv://admin:admin@cluster0.ek4rh.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
//we connect now the mongodb connection
//let's check if successfully connected or running
//Set notification for connection success or failure
let db = mongoose.connection;
//if error occured, output the error in the console.

db.on("error", console.error.bind(console, "connection error"))
//if the connection is successful, output this to the console.
db.once("open", () => console.log("We're connected to the cloud database."))

app.use(express.json());
app.use(express.urlencoded({extended:true}))

//Mongoose Schemas
//Schema determine the structure of the documents to be wtitten in the database
//it acts as a blueprints to out data
//We will use Schema() constructor of the monggose module to create a new Schema object
//sa tuwing gagawa ng schema always may new
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		//Default values are the pre defined values for a field if we don't put any value
		default: "pending"
	}
})
//to allow us to gain access to the schema at the same time so that we can use crud method
//Kapag model dapat laging capital
//first parameter if saan sstore ang data
//2nd parameter name of blueprints schema para mapasok ang data
const Task = mongoose.model("Task", taskSchema)
//Models are what allows us to gain access to methods that will perform CRUD functions.
//Models must be in singular form and capitalized following the MVC approach for naming conventions
//First Parameter of the mongoose model method indicates the collection in where to store the data.
//Second parameter is use to specify the schema or the blueprint

//Routes

//Create a new task
/*
Business Logic
1. Add functionality to check if there are duplicate tasks
	-if the task already exists, return there is a duplicate
	-if the task doesn't exist, we can add it in the database.
2. The task data will be coming from the request's body
3. Create a new Task Object with properties that we need to store the data that we get.
4. Then save the data

*/

app.post("/task", (req, res) => {
	//Check if there are duplicate tasks
	//findOne() is a mongoose method that acts similar to "find"
	//it returns the first document that matches the search criteria
	Task.findOne( {name: req.body.name}, (error, result) => {
		//If a document was found and the document's name matches the information sent via client/position

		if(result !== null && result.name == req.body.name){
			//return a message to the client/postman
			return res.send("Duplicate task found")
		}else{
			//if no document was found
			//Create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name

			})

			newTask.save((saveErr, savedTask) => {
				//if there are errors in saving
				if(saveErr){
					//will print any error found in the console
					//saveErr is an error object that will contain details about the error
					//Errors normally come as an object data type
					return console.error(saveErr)
				}else{

					//no error found while creating the document
					//Return a status code of 201 for successful creation
					return res.status(201).send("New Task Created")
				}
			})



		}




	} )
})

//Day 41 Business logic for retrieving data

/*
1. Retrieve all the documents using the find()
2. If an error is encountered, print the error.
3. If no errors are found, send a success status back to the client and return an array of documents.


*/
app.get("/task", (req, res) => {

	//an empty "{}" means it returns all the document and stores them in the "result" parameter of the call back function
	Task.find( {}, (err, result) => {
		//If an error occured
		if(err){
			return console.log(err)
		} else {
			return res.status(200).json({
				data: result
			})
		}
	} )
})
/*
Business Logic.
Register a User
1. Find if there are duplicate user
 -if user already exists, we reurn an error
  -if user doesn't exist, we add it in the database.
2. if no duplicate found we can add validation.
  -if the username and password are both not blank.
  -if blank, send response "Both" username and password must be provided
  - both condition has been met, create a new object.
  -Save the new object.
  	-if error, return an error message
  	-else, response a status 2001 and "New User Registered" 

*/



const userSchema = new mongoose.Schema({
	username: String,
	password: String
	
})

const User = mongoose.model("User", userSchema)

app.post("/register", (req, res) => {
	
	User.findOne( {username: req.body.username}, (error, result) => {
	
		if(result !== null && result.username == req.body.username){
	
			return res.send("username already exist")
		}else
		{
		//No duplicate found
		if(req.body.username !==  "" && req.body.password !== ""){
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})
		
		
		

		newUser.save((saveErr, savedUser) => {
				//if there are errors in saving
				if(saveErr){
					//will print any error found in the console
					//saveErr is an error object that will contain details about the error
					//Errors normally come as an object data type
					return console.error(saveErr)
				}else{

					//no error found while creating the document
					//Return a status code of 201 for successful creation
					return res.status(201).send("New User Registered")
				}
		
})

		}else {
			return res.send("Both username and password must be completed")
		}

		}


	})
})

app.get("/register", (req, res) => {
	//Find for a document with the matching username
	//an empty "{}" means it returns all the document and stores them in the "result" parameter of the call back function
	User.find({ }, (err, result) => {
		//If an error occured
		if(err){
			return console.log(err)
		} else {
			return res.status(201).json({
				data: result

			})
		}
	} )
})



app.listen(port, ()=> console.log(`Server is running at port ${port}`))

